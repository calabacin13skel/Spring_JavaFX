package eu.calabacin.skel.springwithjavafx.model;

import lombok.Data;

@Data
public class Credentials {
    private String userName;
    private String password;
}
