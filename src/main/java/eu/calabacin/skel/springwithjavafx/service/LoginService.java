package eu.calabacin.skel.springwithjavafx.service;

import org.springframework.stereotype.Component;

@Component
public class LoginService {

    private String userName;

    public boolean login(String userName, String password) {
        if (isValidCredential(userName, password)) {
            this.userName = userName;
            return true;
        }
        return false;
    }

    // This method should call a DAO or any authentication service available
    private boolean isValidCredential(String userName, String password) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return (userName.equalsIgnoreCase("admin") && password.equals("admin"));
    }

    public String getUserName() {
        return userName;
    }
}
