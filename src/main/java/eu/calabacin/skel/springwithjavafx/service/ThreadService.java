package eu.calabacin.skel.springwithjavafx.service;

import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ThreadService {
    @Getter
    private ExecutorService bussinessThreadPool;
    @Getter
    private ExecutorService uiThreadPool;

    @PostConstruct
    private void init() {
        bussinessThreadPool = Executors.newFixedThreadPool(2);
        uiThreadPool = Executors.newFixedThreadPool(2);
    }
}
