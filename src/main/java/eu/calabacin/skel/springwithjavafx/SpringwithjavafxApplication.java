package eu.calabacin.skel.springwithjavafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class SpringwithjavafxApplication extends Application {
    private static String[] savedArgs;
    private ConfigurableApplicationContext springContext;
    private Parent root;

    public static void main(String[] args) {
        savedArgs = args;
        launch(savedArgs);
//        launch(SpringwithjavafxApplication.class, args);
    }

    @Override
    public void init() throws Exception {
        // SpringApplication.run(SpringwithjavafxApplication.class, args);
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringwithjavafxApplication.class);
        this.springContext = builder.run(getParameters().getRaw().toArray(new String[0]));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        createFXMLLoader("/views/Login.fxml");
        createScene(primaryStage, "Hello World", 900, 500);
    }

    @Override
    public void stop() throws Exception {
        this.springContext.close();
        System.gc();
        System.runFinalization();
    }

    private void createScene(Stage primaryStage, String title, int width, int height) {
        Scene scene = new Scene(root, width, height);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void createFXMLLoader(String viewName) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(viewName));
            fxmlLoader.setControllerFactory(this.springContext::getBean);
            root = fxmlLoader.load();
        } catch (IOException e) {
            throw new IllegalStateException("Unable to load view:", e);
        }
    }

}
