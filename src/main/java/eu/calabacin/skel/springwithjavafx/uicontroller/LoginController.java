package eu.calabacin.skel.springwithjavafx.uicontroller;

import eu.calabacin.skel.springwithjavafx.service.LoginService;
import eu.calabacin.skel.springwithjavafx.service.ThreadService;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
@Scope
@Slf4j
public class LoginController {
    private static final int ENTER_KEY = 10;

    public TextField userField;
    public PasswordField passwordField;
    public Button loginButton;

    @Autowired
    private ThreadService threadService;
    @Autowired
    private LoginService loginService;

    public void onButtonClick(ActionEvent actionEvent) {
        CompletableFuture
                .runAsync(() -> attemptLogin(userField.getText(), passwordField.getText()), threadService.getBussinessThreadPool());
    }

    public void onEnterPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode().getCode() == ENTER_KEY) {
            CompletableFuture
                    .runAsync(() -> attemptLogin(userField.getText(), passwordField.getText()), threadService.getBussinessThreadPool());
        }
    }

    private void attemptLogin(String userName, String password) {
        try {
            loginButton.setDisable(true);
            if (loginService.login(userName, password)) {
                // Go to other section of the application
                log.info("Login ok with user {}", userName);

            } else {
                // Show error message
                log.info("Login INVALID for user {}", userName);
            }
        } finally {
            loginButton.setDisable(false);
        }
    }

}
