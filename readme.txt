In order for JavaFX applications to run this option must be specified in Run Configuration:
--module-path {PATH_TO_OPENJFX_LIB} --add-modules javafx.controls,javafx.fxml

For example:
--module-path /usr/share/openjfx/lib --add-modules javafx.controls,javafx.fxml
